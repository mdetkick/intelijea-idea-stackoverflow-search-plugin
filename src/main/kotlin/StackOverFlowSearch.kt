import com.intellij.ide.BrowserUtil
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.openapi.ui.Messages
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

class StackOverFlowSearch : AnAction(){

    override fun actionPerformed(e: AnActionEvent) {
        val selectedText = e.getData(PlatformDataKeys.EDITOR)?.selectionModel?.selectedText
        if (selectedText != null){
            val programmingLanguage = e.getData(PlatformDataKeys.PSI_FILE)?.language.toString()
            val formattedProgrammingLanguage = formatProgrammingLanguage(programmingLanguage)
            val encodedSelectedText = URLEncoder.encode(selectedText, StandardCharsets.UTF_8)
            val encodedProgrammingLanguage = URLEncoder.encode(formattedProgrammingLanguage, StandardCharsets.UTF_8)
            val url = "https://stackoverflow.com/search?q=%5B$encodedProgrammingLanguage%5D+$encodedSelectedText"
            BrowserUtil.browse(url)
        }else{
            Messages.showMessageDialog("Pls select some text","StackOverFlowSearch",Messages.getInformationIcon())
        }
    }

    private fun formatProgrammingLanguage(programmingLanguage:String):String{
        return programmingLanguage.replace(Regex(".*:"),"").toLowerCase()
    }
}